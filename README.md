The DGoSE Modpack is a Minecraft modpack accessed by using the Technic Launcher.

Installation:

1) Make sure [Java 64-bit](https://java.com/en/download/manual.jsp) is installed.  Make cure to click on "Windows Offline (64-bit)"

2) Download the [Technic Launcher](https://www.technicpack.net/download)

3) Copy/paste this link into the search bar:

    http://api.technicpack.net/modpack/miners-techno-magic-pack

4) Allocate more than 1GB of RAM to the modpack by going to:

    Launcher Options -> Java Settings -> Memory

__TO DO:__
* Move to Solder
  * Install on RPi
  * Prepare repo
* Fix ProjectE --> ~50% done; Need user input to continue
* OpenEye --> Awaiting Users
* Make a modpack permissions file (Do during Solder migration)
* Modpack Art:
  * Make a render for modpack background
  * Make "DGoSE" text for logo (alpha around the text so you can see the background)
  * Modpack icon/thumbnail
