Name 460mm AP Shell
ShortName APNavyShell
Model fc.Plasma
//Colour of icon and model
Colour 255 255 255
ItemID 7465
Texture Shell
Icon 460mmShell
//How affected by gravity the bullet is
FallSpeed 0.46
//The number of rounds each item has. Limited to 32000.
RoundsPerItem 1
//The maximum stack size of a stack of these
MaxStackSize 1
//The damage multiplier caused by the bullet on hitting an entity
DamageVsVehicles 2500
DamageVsLiving 800
//Size of explosion caused
Explosion 5
//True if bullet explodes on hitting anything. Fuse denotes the maximum time it may spend in the air before detonating
ExplodeOnImpact True
Fuse 200
FlakParticles 50
//Amount of fire to spread on exploding; for each increment, it will increase the dimensions of the burning area by 2. i.e. 1 will burn a 3x3 square, 2 will burn a 5x5 square
Fire 0
BreaksGlass True
//Size of box to check for entities to hit / detonate
HitBoxSize 1.5
HitSound bomb
//Whether or not the bullet can travel through an entity having hit it
Penetrates True
SmokeTrail True
//True if this is a bomb
Bomb False
Shell False
RecipeOutput 1
Recipe I iron T NavyShell
 I 
 T
 I
