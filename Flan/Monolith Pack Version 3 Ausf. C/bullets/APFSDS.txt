Name Main Battle Tank Ammo
ShortName APFSDS
Model mw.StingerMissile
Texture Stinger
//Colour of icon and model
Colour 255 255 255
ItemID 7447
Icon magicshell
//How affected by gravity the bullet is
FallSpeed 0.2
//The number of rounds each item has. Limited to 32000.
RoundsPerItem 1
//The maximum stack size of a stack of these
MaxStackSize 12
//The damage multiplier caused by the bullet on hitting an entity
DamageVsLiving 20
DamageVsVehicles 606
//Size of explosion caused
Explosion 3
//True if bullet explodes on hitting anything. Fuse denotes the maximum time it may spend in the air before detonating
ExplodeOnImpact True
Fuse 500
FlakParticles 25
//Amount of fire to spread on exploding; for each increment, it will increase the dimensions of the burning area by 2. i.e. 1 will burn a 3x3 square, 2 will burn a 5x5 square
Fire 0
BreaksGlass True
//Size of box to check for entities to hit / detonate
HitBoxSize 0.12
HitSound bomb
//Whether or not the bullet can travel through an entity having hit it
Penetrates False
SmokeTrail True
LockOnToDriveables False
LockOnToPlayers False
LockOnToLivings False
MaxLockOnAngle 0.0
//True if this is a bomb
Bomb False
Shell True
RecipeOutput 4
Recipe I shell T tnt N INugget F 27135_Part_Misc_Ammo_Fuse
 I
NTN
NFN
Description 125mm Armor-piercing fin-stabilized discarding-sabot shell