Name 127mm Modern Shell
ShortName 127mmBAE
Model fc.Plasma
Texture Shell
//Colour of icon and model
Colour 255 255 255
ItemID 7447
Icon BattleshipShell
//How affected by gravity the bullet is
FallSpeed 0.05
//The number of rounds each item has. Limited to 32000.
RoundsPerItem 20
//The maximum stack size of a stack of these
MaxStackSize 4
//The damage multiplier caused by the bullet on hitting an entity
DamageVsLiving 100
DamageVsVehicles 3770
//Size of explosion caused
Explosion 4
//True if bullet explodes on hitting anything. Fuse denotes the maximum time it may spend in the air before detonating
ExplodeOnImpact True
Fuse 20
FlakParticles 150
FlakParticleType largeexplode
//Amount of fire to spread on exploding; for each increment, it will increase the dimensions of the burning area by 2. i.e. 1 will burn a 3x3 square, 2 will burn a 5x5 square
Fire 0
BreaksGlass True
//Size of box to check for entities to hit / detonate
HitBoxSize 1
HitSound bomb
//Whether or not the bullet can travel through an entity having hit it
Penetrates False
SmokeTrail True
TrailParticleType largesmoke
LockOnToDriveables False
LockOnToPlayers False
LockOnToLivings False
MaxLockOnAngle 0.0
//True if this is a bomb
Bomb False
Shell True
Description ammo for Mark 45 Naval Gun