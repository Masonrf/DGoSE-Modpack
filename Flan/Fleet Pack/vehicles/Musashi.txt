// ------------------------------------------------------ Naming ------------------------------------------------------
Name Musashi
ShortName Musashi
ItemID 7464
// ------------------------------------------------------ Visuals ------------------------------------------------------
Icon MusashiIcon
Model monolith.Musashi
Texture SkinMusashi
//Third Person Camera Distance
CameraDistance 65.0
// ------------------------------------------------------ Movement ------------------------------------------------------
//Throttle
MaxThrottle 0.3
MaxNegativeThrottle 0.1
//Steering modifiers
TurnLeftSpeed 0.4
TurnRightSpeed 0.4
Drag 10.0
//For calculating whether wheels are on the ground or not for driving
WheelRadius 1.0
//Wheel positions for collision handling. Tanks still have 4 wheels, they are just the corner ones.
WheelPosition 0 -102 -35 -48
WheelPosition 1 -102 -35 48
WheelPosition 2 600 -35 48
WheelPosition 3 600 -35 -48
WheelSpringStrength 0.5
//If true, then all wheels will apply drive forces
FourWheelDrive false
// ------------------------------------------------------ Weaponry ------------------------------------------------------
//Weapon types. Options are Missile, Bomb, Gun, Shell, None
Primary Shell
Secondary Gun
//Time between shots in 1/20ths of seconds
ShootDelayPrimary 6
ShootDelaySecondary 1
//Whether to alternate or fire all together
AlternatePrimary True
AlternateSecondary False
//Firing modes. One of SemiAuto, FullAuto or Minigun
ModePrimary FullAuto
ModeSecondary FullAuto
//Add shoot origins. These are the points on your vehicle from which bullets / missiles / shells / bombs appear
ShootPointPrimary 190 20 60 turret
ShootPointPrimary 190 20 -60 turret
ShootPointPrimary 260 20 50 turret
ShootPointPrimary 260 20 -50 turret
ShootPointPrimary 30 20 70 turret
ShootPointPrimary 30 20 -70 turret
ShootPointPrimary -230 20 40 turret
ShootPointPrimary -230 20 -40 turret
ShootPointPrimary 80 20 80 turret
ShootPointPrimary 80 20 -80 turret
ShootPointPrimary -30 20 90 turret
ShootPointPrimary -30 20 -90 turret
ShootPointSecondary 330 55 -20 turret lightFlak
ShootPointSecondary 130 25 30 turret lightFlak
ShootPointSecondary -100 5 -30 turret lightFlak
ShootPointSecondary -50 25 10 turret lightFlak
ShootPointSecondary 50 65 -50 turret lightFlak
ShootPointSecondary 200 75 70 turret lightFlak
ShootPointSecondary 330 50 -70 turret lightFlak
ShootPointSecondary 130 20 50 turret lightFlak
ShootPointSecondary -100 20 -80 turret lightFlak
ShootPointSecondary -50 45 80 turret lightFlak
// ------------------------------------------------------ Inventory ------------------------------------------------------
CargoSlots 0
BombSlots 0
MissileSlots 8
AllowAllAmmo False
AddAmmo 12.7cmShellAA
//Fuel Tank Size (1 point of fuel will keep one propeller going with throttle at 1 for 1 tick)
FuelTankSize 2000
// ------------------------------------------------------ Passengers ------------------------------------------------------
Driver -19 150 0 -360 360 -3 75
Passengers 5
Passenger 1 133 30 0 core -135 135 -2 35 460mmFLEET PassengerGun1
Passenger 2 248 13 0 core -135 135 -2 35 460mmFLEET PassengerGun2
Passenger 3 -306 21 0 core 45 315 -2 35 460mmFLEET PassengerGun3
Passenger 4 57 48 0 core -135 135 -5 35 155X3 PassengerGun4
Passenger 5 -233 46 0 core 45 315 -5 35 155X3 PassengerGun5
GunOrigin 1 133 25 0
GunOrigin 2 248 8 0
GunOrigin 3 -306 16 0
GunOrigin 4 57 43 0
GunOrigin 5 -233 41 0
// ------------------------------------------------------- Sounds -------------------------------------------------------
StartSound EngineStart
StartSoundLength 24
EngineSound CruiserEngine
EngineSoundLength 15
ShootSoundPrimary 12cmFire
ShootSoundSecondary NavyFlak
//Recipe
//Each section of the plane may have many parts
//The sections are tailWheel, tail, bay, topWing, leftWingWheel, leftWing, rightWingWheel,
//rightWing, nose, turret, coreWheel, core
AddRecipeParts core 400 FleetFuel 180 FleetAmmo 700 FleetSteel 100 FleetAluminum
//Dye colours are "black", "red", "green", "brown", "blue", "purple", "cyan", "silver", "gray", "pink", "lime", "yellow", "lightBlue", "magenta", "orange", "white"
AddDye 24 red
AddDye 48 black
//Health and collision
SetupPart core 31999 -410 -70 -70 1000 110 140
BulletDetection 20
ModelScale 3.1
Description Yamato class heavy Battleship
Boat
PlaceableOnLand False
PlaceableOnWater True
FloatOnWater True
WheelStepHeight 0
Buoyancy 0.08
CollisionPoint -102 -92 -48 core
CollisionPoint -102 -92 48 core
CollisionPoint 500 -92 -24 core
CollisionPoint 500 -92 24 core
CollisionPoint -102 -92 -24 core
CollisionPoint -102 -92 24 core
CollisionPoint 600 -92 -48 core
CollisionPoint 600 -92 48 core
CollisionPoint 100 -92 -48 core
CollisionPoint 100 -92 48 core
CollisionPoint 600 -92 -24 core
CollisionPoint 600 -92 24 core
CollisionPoint 102 -92 -24 core
CollisionPoint 102 -92 24 core
CollisionPoint 700 -92 -48 core
CollisionPoint 700 -92 48 core
CollisionPoint 300 -92 -48 core
CollisionPoint 300 -92 48 core
CollisionPoint 700 -92 -24 core
CollisionPoint 700 -92 24 core
CollisionPoint 302 -92 -24 core
CollisionPoint 302 -92 24 core
CollisionPoint 400 -92 -48 core
CollisionPoint 400 -92 48 core
CollisionPoint 200 -92 -48 core
CollisionPoint 200 -92 48 core
CollisionPoint 400 -92 -24 core
CollisionPoint 400 -92 24 core
CollisionPoint 202 -92 -24 core
CollisionPoint 202 -92 24 core