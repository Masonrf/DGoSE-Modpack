// ------------------------------------------------------ Naming ------------------------------------------------------
Name DDG 9 Musashi
ShortName Microsoft
ItemID 7464
// ------------------------------------------------------ Visuals ------------------------------------------------------
Icon IconMicrosoft
Model monolith.Microsoft
Texture SkinMicrosoft
//Third Person Camera Distance
CameraDistance 65.0
// ------------------------------------------------------ Movement ------------------------------------------------------
//Throttle
MaxThrottle 0.4
MaxNegativeThrottle 0.1
//Steering modifiers
TurnLeftSpeed 0.45
TurnRightSpeed 0.45
Drag 10.0
//For calculating whether wheels are on the ground or not for driving
WheelRadius 1.0
//Wheel positions for collision handling. Tanks still have 4 wheels, they are just the corner ones.
WheelPosition 0 -102 -35 -48
WheelPosition 1 -102 -35 48
WheelPosition 2 600 -35 48
WheelPosition 3 600 -35 -48
WheelSpringStrength 0.5
//If true, then all wheels will apply drive forces
FourWheelDrive false
// ------------------------------------------------------ Weaponry ------------------------------------------------------
//Weapon types. Options are Missile, Bomb, Gun, Shell, None
Primary Shell
Secondary Gun
//Time between shots in 1/20ths of seconds
ShootDelayPrimary 20
ShootDelaySecondary 2
//Whether to alternate or fire all together
AlternatePrimary True
AlternateSecondary False
//Firing modes. One of SemiAuto, FullAuto or Minigun
ModePrimary FullAuto
ModeSecondary FullAuto
//Add shoot origins. These are the points on your vehicle from which bullets / missiles / shells / bombs appear
ShootPointPrimary 190 20 60 turret
ShootPointPrimary 190 20 -60 turret
ShootPointPrimary 260 20 50 turret
ShootPointPrimary 260 20 -50 turret
ShootPointPrimary 30 20 70 turret
ShootPointPrimary 30 20 -70 turret
ShootPointPrimary -230 20 40 turret
ShootPointPrimary -230 20 -40 turret
ShootPointPrimary 80 20 80 turret
ShootPointPrimary 80 20 -80 turret
ShootPointPrimary -30 20 90 turret
ShootPointPrimary -30 20 -90 turret
ShootPointSecondary -100 35 -30 turret goalKeeper
ShootPointSecondary -200 45 30 turret goalKeeper
ShootPointSecondary -200 45 -30 turret goalKeeper
ShootPointSecondary -100 35 30 turret goalKeeper
// ------------------------------------------------------ Inventory ------------------------------------------------------
CargoSlots 0
BombSlots 0
MissileSlots 8
AllowAllAmmo False
AddAmmo SeaSparrow
//Fuel Tank Size (1 point of fuel will keep one propeller going with throttle at 1 for 1 tick)
FuelTankSize 2000
// ------------------------------------------------------ Passengers ------------------------------------------------------
Driver -17 140 0 -180 180 -5 80
Passengers 9
Passenger 1 139 44 0 core -135 135 -2 35 Railgun PassengerGun1
Passenger 2 250 27 0 core -135 135 -2 35 Railgun PassengerGun2
Passenger 3 -315 33 0 core 45 315 -2 35 Railgun PassengerGun3
Passenger 4 59 63 0 core -135 135 -5 35 Vista PassengerGun4
Passenger 5 -236 58 0 core 45 315 -5 35 Vista PassengerGun5
Passenger 6 -69 28 72 core 0 180 -5 35 Vista PassengerGun6
Passenger 7 -69 28 -72 core 180 360 -5 35 Vista PassengerGun7
Passenger 8 406 -8 0 core -360 360 85 95 VLS PassengerGun8
Passenger 9 454 -8 0 core -360 360 85 95 VLS PassengerGun9
GunOrigin 1 133 25 0
GunOrigin 2 248 8 0
GunOrigin 3 -306 16 0
GunOrigin 4 57 43 0
GunOrigin 5 -233 41 0
GunOrigin 6 -69 18 72
GunOrigin 7 -69 18 -72
GunOrigin 8 406 -12 0 
GunOrigin 9 454 -12 0 
// ------------------------------------------------------- Sounds -------------------------------------------------------
StartSound EngineStart
StartSoundLength 24
EngineSound ModernEngine
EngineSoundLength 5
ShootSoundPrimary RIMshoot
ShootSoundSecondary GoalKeeperShoot
//Recipe
//Each section of the plane may have many parts
//The sections are tailWheel, tail, bay, topWing, leftWingWheel, leftWing, rightWingWheel,
//rightWing, nose, turret, coreWheel, core
AddRecipeParts core 500 FleetFuel 200 FleetAmmo 700 FleetSteel 999 FleetAluminum
//Dye colours are "black", "red", "green", "brown", "blue", "purple", "cyan", "silver", "gray", "pink", "lime", "yellow", "lightBlue", "magenta", "orange", "white"
AddDye 24 red
AddDye 48 gray
//Health and collision
SetupPart core 31999 -410 -70 -70 1000 110 140
BulletDetection 20
ModelScale 3.1
Description Paul Allen Class Super-heavy Guided Missile Cruiser_Powered By Windows 9 Technology
Boat
PlaceableOnLand False
PlaceableOnWater True
FloatOnWater True
WheelStepHeight 0
Buoyancy 0.08
CollisionPoint -102 -92 -48 core
CollisionPoint -102 -92 48 core
CollisionPoint 500 -92 -24 core
CollisionPoint 500 -92 24 core
CollisionPoint -102 -92 -24 core
CollisionPoint -102 -92 24 core
CollisionPoint 600 -92 -48 core
CollisionPoint 600 -92 48 core
CollisionPoint 100 -92 -48 core
CollisionPoint 100 -92 48 core
CollisionPoint 600 -92 -24 core
CollisionPoint 600 -92 24 core
CollisionPoint 102 -92 -24 core
CollisionPoint 102 -92 24 core
CollisionPoint 700 -92 -48 core
CollisionPoint 700 -92 48 core
CollisionPoint 300 -92 -48 core
CollisionPoint 300 -92 48 core
CollisionPoint 700 -92 -24 core
CollisionPoint 700 -92 24 core
CollisionPoint 302 -92 -24 core
CollisionPoint 302 -92 24 core
CollisionPoint 400 -92 -48 core
CollisionPoint 400 -92 48 core
CollisionPoint 200 -92 -48 core
CollisionPoint 200 -92 48 core
CollisionPoint 400 -92 -24 core
CollisionPoint 400 -92 24 core
CollisionPoint 202 -92 -24 core
CollisionPoint 202 -92 24 core