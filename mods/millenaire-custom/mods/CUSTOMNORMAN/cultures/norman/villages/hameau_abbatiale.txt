name:Hameau abbatial
type:hameau
weight:0
qualifier:l'église
qualifier:l'abbaye
radius:40

//starting buildings:
centre:abbey
start:glassmaker
start:lumbermanhut2
start:guesthouse
start:cloister
start:grove

player:playersmallfield
player:playersmallhouse

pathmaterial:pathdirt
pathmaterial:pathgravel
pathmaterial:pathslabs

//to be built in priority
core:cattlefarm2
core:bakery
core:farm2

//to be built after the core ones
secondary:sheepchickenfarm2
secondary:well

//never for this type of village
never:alchimie
never:cathedrale
never:lumbermanhut
never:farm
never:cattlefarm
never:quarry
never:pigfarm
never:sheepchickenfarm
never:carpenterhouse

//Selling prices:
sellingPrice:bookshelves,14
sellingPrice:parchmentVillagers,5
sellingPrice:parchmentBuildings,5
sellingPrice:parchmentItems,5
sellingPrice:parchmentComplete,32
sellingPrice:tripes,1/48
sellingPrice:bread,15
sellingPrice:egg,18
sellingPrice:wool_white,7
sellingPrice:timberframe_plain,40
sellingPrice:timberframe_cross,56

//Buying prices:
buyingPrice:bookshelves,12
buyingPrice:wool_white,5
buyingPrice:leather,15
buyingPrice:normanAxe,10/10
buyingPrice:normanHoe,10/10
buyingPrice:timberframe_plain,36
buyingPrice:timberframe_cross,52