native_name=Stableman
gender=male
familyNameList=family_names
firstNameList=men_names

texture=/textures/entity/norman/ML_stableman0.png

goal=gorest
goal=gosocialise
goal=chat
goal=gopray
goal=goDrink

goal=makesaddle
goal=makenametag
goal=makelead
goal=makehorsearmor

requiredGood=iron,5
requiredGood=leather,5
requiredGood=paper,1
requiredGood=dye_black,1
requiredGood=wool_white,3

baseAttackStrength=5
tag=helpInAttacks
defaultweapon=woodaxe
experiencegiven=5
