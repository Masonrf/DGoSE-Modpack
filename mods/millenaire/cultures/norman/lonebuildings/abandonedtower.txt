name:Tour abandonnée
weight:10
radius:10

biome:forest
biome:plains
biome:taiga
biome:extreme hills
biome:foresthills

//extraBiomes mod
biome:forested island
biome:autumn woods
biome:green hills
biome:forested hills

//plenty o'biomes mod
biome:garden
biome:grassland
biome:highland
biome:meadow
biome:mountain
biome:orchard
biome:boreal forest
biome:grove
biome:seasonal forest
biome:woodland

biome:wasteland
biome:dead forest
biome:drylands
//starting buildings:
centre:abandonedtower