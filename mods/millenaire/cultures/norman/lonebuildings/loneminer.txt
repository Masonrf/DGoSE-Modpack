name:Mineur solitaire
weight:10
radius:10
biome:extreme hills
biome:ice mountains

//plenty o'biomes mod
biome:garden
biome:grassland
biome:highland
biome:meadow
biome:mountain
biome:orchard
biome:boreal forest
biome:grove
biome:seasonal forest
biome:woodland
biome:snow forest

//starting buildings:
centre:lonemine