name:Château félon
weight:4
radius:20
carriesraid:true

biome:forest
biome:plains
biome:taiga
biome:extreme hills
biome:foresthills

//extraBiomes mod

biome:forested island
biome:autumn woods
biome:green hills
biome:forested hills

//plenty o'biomes mod
biome:garden
biome:grassland
biome:highland
biome:meadow
biome:mountain
biome:orchard
biome:boreal forest
biome:grove
biome:seasonal forest
biome:woodland

//starting buildings:
centre:feloncastle